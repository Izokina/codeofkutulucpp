#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <chrono>
#include <unordered_map>
#include <math.h>
#include <string.h>
#include <stdio.h>

using Chrono = std::chrono::high_resolution_clock;
using Time = Chrono::duration::rep;

class Timer {
private:
	Chrono::time_point start = Chrono::now();

public:
	Time get() const {
		auto finish = Chrono::now();
		return std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start).count();
	}
};

class Alarm : public Timer {
private:
	Time ttl;

public:
	Alarm(Time ttl) : ttl(ttl) { }

	bool finish() const {
		return get() >= ttl;
	}

	double rate() const {
		return get() * 1. / ttl;
	}
};

using Point = uint16_t;

using Id = int16_t;
using Dist = int16_t;
using Version = uint32_t;
using Tick = uint8_t;
using Hp = int16_t;
using Amount = int8_t;

constexpr Point MAX_WIDTH = 24;
constexpr Point MAX_HEIGHT = 20;
constexpr int MAX_SIZE = MAX_WIDTH * MAX_HEIGHT;

template<typename T>
using Field = T[MAX_SIZE];

enum class Cell : uint8_t {
	WALL = '#',
	FLOOR = '.',
	SPAWN = 'w',
	SHELTER = 'U',
};

Field<Cell> field;
Hp lossGroup;
Hp lossAlone;
Tick wandererSpawn;
Tick wandererTtl;

enum Step : int8_t {
	WAIT = 0,
	UP = (int8_t) -MAX_WIDTH,
	RIGHT = 1,
	DOWN = (int8_t) MAX_WIDTH,
	LEFT = -1,
};

constexpr Step STEPS[] = { UP, RIGHT, DOWN, LEFT };
constexpr Step MOVES[] = { WAIT, UP, RIGHT, DOWN, LEFT };

Point parse(int x, int y) {
	return y * MAX_WIDTH + x;
}

std::pair<int, int> unparse(Point p) {
	return { p % MAX_WIDTH, p / MAX_WIDTH };
}

Dist rawDist(Point a, Point b) {
	auto aa = unparse(a);
	auto bb = unparse(b);
	return abs(aa.first - bb.first) + abs(aa.second - bb.second);
}

template<typename F>
Version bfs(Field<Dist> dst, Field<Version> dstVersion, Point start, const F& heavy) {
	static Version version = 0;
	static std::vector<Point> queue[8];

	version++;
	size_t vertices = 1;
	queue[0].push_back(start);
	Dist l = 0;
	while (vertices > 0) {
		auto& q = queue[l & 7];
		auto& q1 = queue[(l + 1) & 7];
		auto& q5 = queue[(l + 5) & 7];
		for (Point pos : q) {
			if (dstVersion[pos] == version)
				continue;
			dst[pos] = l;
			dstVersion[pos] = version;
			for (auto s : STEPS) {
				Point newPos = pos + s;
				if (field[newPos] == Cell::WALL)
					continue;
				vertices++;
				(heavy(newPos) ? q5 : q1).push_back(newPos);
			}
		}
		vertices -= q.size();
		q.clear();
		l++;
	}
	return version;
}

template<typename F>
void bfs(Field<Dist> dst, Point start, const F& heavy) {
	static Field<Version> defaultDstVersion;

	bfs(dst, defaultDstVersion, start, heavy);
}

class Grid {
private:
	Field<Field<Dist>> dstCache;
	Field<Field<Field<Dist>>> lightCache;
	Field<Field<bool>> losCache;
	Field<Dist> dst;
	Field<Version> version;
	std::vector<Point> spawnPoints;
	std::vector<Point> shelterPoints;

public:
	void init(int height, int width) {
		Alarm initer(800 * 1000 * 1000);
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				Point pos = parse(x, y);
				if (field[pos] == Cell::SPAWN)
					spawnPoints.push_back(pos);
				if (field[pos] == Cell::SHELTER)
					shelterPoints.push_back(pos);
				for (Step s : STEPS) {
					Point p = pos;
					while (field[p] != Cell::WALL) {
						losCache[pos][p] = true;
						p += s;
					}
				}
			}
		}
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				Point pos = parse(x, y);
				memset(dstCache[pos], -1, height * MAX_WIDTH * sizeof(Dist));
				if (field[pos] == Cell::WALL)
					continue;
				bfs(dstCache[pos], pos, [](Point) { return false; });
			}
		}
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				Point pos = parse(x, y);
				if (field[pos] == Cell::WALL)
					continue;
				auto c = dstCache[pos];
				auto f = [=](Point p) {
					Dist d = c[p];
					return d >= 0 && d <= 5;
				};
				for (int yy = 0; yy < height; yy++) {
					for (int xx = 0; xx < width; xx++) {
						Point p = parse(xx, yy);
						if (field[p] == Cell::WALL)
							continue;
						bfs(lightCache[pos][p], p, f);
					}
				}
				if (initer.finish())
					break;
			}
		}
		Time t = initer.get();
		fprintf(stderr, "Init time: %3d %03d %03d\n", (int)(t / 1000000), (int)(t / 1000 % 1000), (int)(t % 1000));
	}

	bool simpleDist(Point a, Point b, Dist& d) const {
		d = dstCache[a][b];
		return d != -1;
	}

	bool dist(Point a, Point b, Dist& d, const std::vector<Point>& lights) {
		if (!simpleDist(a, b, d))
			return false;
		for (Point l : lights) {
			Dist ld = lightCache[l][a][b];
			d = std::max(d, ld);
		}
		return true;
	}

	bool los(Point a, Point b) {
		return losCache[a][b];
	}

	const std::vector<Point>& spawns() {
		return spawnPoints;
	}

	const std::vector<Point>& shelters() {
		return shelterPoints;
	}
} grid;

enum class BitchType : uint8_t {
	WANDERER,
	SLASHER,
};

enum class BitchState : uint8_t {
	SPAWNING,
	WANDERING,
	STALKING,
	RUSHING,
	STUNNED,
};

enum class Effect : uint8_t {
	NONE,
	PLAN,
	LIGHT,
	YELL,
};

struct Guy {
	Id id;
	Point pos;
	Hp hp;
	Amount plans;
	Amount lights;
	Tick yelledTtl;
	Effect currentEffect;
	Tick effectTtl;
	bool spawnedSlasher;
};

struct Bitch {
	Id id;
	Point pos;
	Id target;
	Tick ttl;
	BitchState state;
};

struct Slasher : Bitch {
	Point targetPos;

	Slasher(const Bitch& b = { }, const Point p = { }) : Bitch(b), targetPos(p) { }
};

struct Shelter {
	Point pos;
	Amount energy;
};

struct State {
private:
	static std::unordered_map<Id, size_t> indices;

public:
	Tick tick;
	Guy guys[4];
	uint16_t yellMask;
	Id nextId;
	std::vector<Slasher> slashers;
	std::vector<Bitch> wanderers;
	std::vector<Shelter> shelters;

	size_t getGuyIdx(Id id) const {
		auto gi = indices.find(id);
		if (gi != indices.end())
			return gi->second;
		size_t idx;
		idx = indices.size();
		if (idx > 3)
			throw "NO MORE!";
		indices[id] = idx;
		return idx;
	}

	Guy& getGuy(Id id) {
		return guys[getGuyIdx(id)];
	}

	const Guy& getGuy(Id id) const {
		return guys[getGuyIdx(id)];
	}
};

std::unordered_map<Id, size_t> State::indices { };

struct Move {
	Step offset;
	Effect effect;
};

bool stalk(Slasher& s, const State& state) {
	if (s.target != -1) {
		auto& g = state.getGuy(s.target);
		if (grid.los(s.pos, g.pos)) {
			s.targetPos = g.pos;
			return true;
		}
	}
	Dist dist = MAX_SIZE;
	bool found = false;
	Id id;
	Point pos;
	for (size_t i = 0; i < 4; i++) {
		auto& g = state.guys[i];
		if (g.hp <= 0)
			continue;
		if (!grid.los(s.pos, g.pos))
			continue;
		Dist d = rawDist(s.pos, g.pos);
		if (d < dist) {
			dist = d;
			found = true;
			id = g.id;
			pos = g.pos;
		}
		else if (d == dist) {
			id = -1;
		}
	}
	if (found) {
		if (id == -1 || dist == 0) {
			s.target = -1;
			s.state = BitchState::STUNNED;
			s.ttl = state.tick + 6;
			return false;
		}
		else {
			s.target = id;
			s.targetPos = pos;
			return true;
		}
	}
	return false;
}

void moveBitch(Bitch& b, const State& state, Step W_STEPS[5], const std::vector<Point>& lights) {
	size_t newTarget;
	Dist dist = MAX_SIZE;
	for (size_t j = 0; j < 4; j++) {
		auto& g = state.guys[j];
		if (g.hp <= 0)
			continue;
		Dist d;
		if (grid.dist(b.pos, g.pos, d, lights)) {
			if (d < dist || (d == dist && g.id == b.target)) {
				newTarget = j;
				dist = d;
			}
		}
	}
	auto& g = state.guys[newTarget];
	b.target = g.id;
	Step step = Step::WAIT;
	for (size_t i = 0; i < 5; i++) {
		Step s = W_STEPS[i];
		Point newPos = b.pos + s;
		if (field[newPos] == Cell::WALL)
			continue;
		Dist d;
		if (grid.dist(newPos, g.pos, d, lights)) {
			if (d < dist) {
				dist = d;
				step = s;
			}
		}
	}
	b.pos += step;
}

void simulate(State& state, const Move moves[4]) {
	state.tick++;
	bool finish = true;
	for (size_t i = 0; i < 4; i++) {
		if (state.guys[i].hp > 0)
			finish = false;
	}
	if (finish)
		return;
	// spawn bitches
	for (auto& g : state.guys) {
		if (g.hp == 0 || g.spawnedSlasher || g.hp >= 200)
			continue;
		g.spawnedSlasher = true;
		state.slashers.push_back({{
			state.nextId++,
			g.pos,
			g.id,
			(Tick)(state.tick + 6 - 1),
			BitchState::SPAWNING
		}});
	}
	if (state.tick % 5 == 0) {
		Dist dist = 0;
		for (Point pos : grid.spawns()) {
			Dist d = MAX_SIZE;
			for (auto& g : state.guys) {
				if (g.hp == 0)
					continue;
				d = std::min(d, rawDist(pos, g.pos));
			}
			dist = std::max(dist, d);
		}
		for (Point pos : grid.spawns()) {
			Dist d = MAX_SIZE;
			for (auto& g : state.guys) {
				if (g.hp == 0)
					continue;
				d = std::min(d, rawDist(pos, g.pos));
			}
			if (dist == d) {
				state.wanderers.push_back({
					state.nextId++,
					pos,
					-1,
					(Tick) (state.tick + wandererSpawn - 1),
					BitchState::SPAWNING
				});
			}
		}
	}
	// spawn shelters
	if (state.tick > 0 && state.tick % 50 == 0) {
		state.shelters.clear();
		for (Point p : grid.shelters())
			state.shelters.push_back({ p, 10 });
	}
	// yell'em all
	bool able[4];
	for (size_t i = 0; i < 4; i++) {
		auto& g = state.guys[i];
		able[i] = (g.hp > 0 && g.yelledTtl <= state.tick);
	}
	for (size_t i = 0; i < 4; i++) {
		if (!able[i] || moves[i].effect != Effect::YELL)
			continue;
		auto& g = state.guys[i];
		for (auto& gg : state.guys) {
			if (g.id == gg.id || rawDist(g.pos, gg.pos) > 1)
				continue;
			uint16_t mask = 1 << (state.getGuyIdx(g.id) * 4 + state.getGuyIdx(gg.id));
			if ((state.yellMask & mask) != 0)
				continue;
			gg.yelledTtl = state.tick + 2;
			state.yellMask |= mask;
		}
	}
	// move guys
	for (size_t i = 0; i < 4; i++) {
		auto& g = state.guys[i];
		if (g.hp <= 0 || g.yelledTtl > state.tick)
			continue;
		Point newPos = g.pos + moves[i].offset;
		if (field[newPos] != Cell::WALL)
			g.pos = newPos;
	}
	// apply effects
	for (size_t i = 0; i < 4; i++) {
		auto& g = state.guys[i];
		if (g.hp == 0)
			continue;
		Effect newEffect = moves[i].effect;
		if (newEffect == Effect::NONE || newEffect == Effect::YELL || g.yelledTtl > state.tick || g.currentEffect != Effect::NONE)
			continue;
		switch (newEffect) {
		case Effect::PLAN:
			if (g.plans == 0)
				continue;
			g.effectTtl = state.tick + 4;
			g.plans--;
			break;
		case Effect::LIGHT:
			if (g.lights == 0)
				continue;
			g.effectTtl = state.tick + 2;
			g.lights--;
			break;
		default:
			continue;
		}
		g.currentEffect = newEffect;
	}
	// light their path
	std::vector<Point> lights;
	for (auto& g : state.guys) {
		if (g.currentEffect != Effect::LIGHT || g.effectTtl <= state.tick)
			continue;
		lights.push_back(g.pos);
	}
	// smoke weed
	Hp heal[] = { 0, 0, 0, 0 };
	for (size_t i = 0; i < 4; i++) {
		auto& g = state.guys[i];
		if (g.currentEffect != Effect::PLAN || g.effectTtl <= state.tick)
			continue;
		if (g.hp != 0)
			heal[i] -= 3;
		for (size_t j = 0; j < 4; j++) {
			auto& gg = state.guys[j];
			Dist d;
			if (gg.hp == 0 || !grid.simpleDist(g.pos, gg.pos, d))
				continue;
			if (d > 2)
				continue;
			heal[i] += 3;
			heal[j] += 3;
		}
	}
	for (size_t i = 0; i < state.shelters.size(); i++) {
		auto& s = state.shelters[i];
		for (size_t j = 0; j < 4; j++) {
			auto& g = state.guys[j];
			if (g.hp == 0 || g.pos != s.pos)
				continue;
			g.hp += 5;
			s.energy--;
		}
		if (s.energy <= 0) {
			state.shelters[i] = state.shelters.back();
			state.shelters.pop_back();
			i--;
		}
	}
	// finalize effects
	for (size_t i = 0; i < 4; i++) {
		auto& g = state.guys[i];
		if (g.hp > 0)
			g.hp = std::min(g.hp + heal[i], 250);
	}
	// wipe heal
	for (size_t i = 0; i < 4; i++)
		heal[i] = 0;
	// move wanderers
	Step W_STEPS[5] = { Step::WAIT };
	for (size_t i = 0; i < 4; i++)
		W_STEPS[i + 1] = STEPS[(state.tick + i) % 4];
	for (size_t i = 0; i < state.wanderers.size(); i++) {
		auto& w = state.wanderers[i];
		if (w.state != BitchState::WANDERING)
			continue;
		moveBitch(w, state, W_STEPS, lights);
	}
	// puhish guys
	for (size_t i = 0; i < 4; i++) {
		auto& g = state.guys[i];
		if (g.hp <= 0)
			continue;
		for (auto& w : state.wanderers) {
			if (w.state == BitchState::WANDERING && w.pos == g.pos)
				heal[i] -= 20;
		}
		bool alone = true;
		for (auto& gg : state.guys) {
			if (gg.hp == 0 || gg.id == g.id)
				continue;
			if (rawDist(g.pos, gg.pos) <= 2)
				alone = false;
		}
		heal[i] -= alone ? lossAlone : lossGroup;
		if (g.hp < 0)
			g.hp = 0;
	}
	// play slashers
	for (size_t i = 0; i < state.slashers.size(); i++) {
		auto& s = state.slashers[i];
		switch (s.state) {
		case BitchState::SPAWNING: {
			if (s.ttl == state.tick) {
				s.state = BitchState::RUSHING;
				s.targetPos = s.pos;
			}
			break;
		}
		case BitchState::WANDERING: {
			if (stalk(s, state)) {
				s.state = BitchState::STALKING;
				s.ttl = state.tick + 2;
				break;
			}
			if (s.state == BitchState::STUNNED)
				break;
			s.target = -1;
			moveBitch(s, state, W_STEPS, lights);
			break;
		}
		case BitchState::STALKING: {
			if (!stalk(s, state))
				s.target = -1;
			if (s.ttl == state.tick)
				s.state = BitchState::RUSHING;
			break;
		}
		case BitchState::RUSHING: {
			if (s.ttl == state.tick)
				break;
			stalk(s, state);
			if (s.state == BitchState::STUNNED)
				break;
			s.pos = s.targetPos;
			s.target = -1;
			s.state = BitchState::STUNNED;
			s.ttl = state.tick + 6;
			for (size_t j = 0; j < 4; j++) {
				auto& g = state.guys[j];
				if (g.hp <= 0)
					continue;
				if (g.pos == s.pos)
					heal[j] -= 20;
			}
			break;
		}
		case BitchState::STUNNED: {
			if (s.ttl == state.tick)
				s.state = BitchState::WANDERING;
			break;
		}
		}
	}
	// finalize punishment
	for (size_t i = 0; i < 4; i++) {
		auto& g = state.guys[i];
		g.hp = std::max(0, g.hp + heal[i]);
	}
	// effects fade away
	for (auto& g : state.guys) {
		if (g.effectTtl <= state.tick)
			g.currentEffect = Effect::NONE;
	}
	// waste wanderers
	for (size_t i = 0; i < state.wanderers.size(); i++) {
		auto& w = state.wanderers[i];
		if (w.state == BitchState::SPAWNING) {
			if (w.ttl == state.tick) {
				w.state = BitchState::WANDERING;
				w.ttl = state.tick + wandererTtl;
			}
			continue;
		}
		if (w.state == BitchState::WANDERING && state.tick == w.ttl) {
			state.wanderers[i] = state.wanderers.back();
			state.wanderers.pop_back();
			i--;
			continue;
		}
		for (size_t j = 0; j < 4; j++) {
			auto& g = state.guys[j];
			if (g.hp <= 0)
				continue;
			if (g.pos == w.pos) {
				state.wanderers[i] = state.wanderers.back();
				state.wanderers.pop_back();
				i--;
				break;
			}
		}
	}
}

struct EmptyStrategy {
	Move solve(const State&, size_t) const {
		return { Step::WAIT, Effect::NONE };
	}
};

struct Idiot {
	Move solve(const State& state, size_t idx) const {
		auto& me = state.guys[idx];
		Move move { };
		bool foundSafe = false;
		Dist bitchDist = 1;
		Dist friendDist = MAX_SIZE;
		for (Step s : MOVES) {
			Point pos = me.pos + s;
			if (field[pos] == Cell::WALL)
				continue;
			Dist bd = MAX_SIZE;
			Dist fd = MAX_SIZE;
			for (auto& w : state.wanderers) {
				Dist d;
				if (grid.simpleDist(pos, w.pos, d))
					bd = std::min(bd, d);
			}
			for (auto& g : state.guys) {
				if (g.id == me.id)
					continue;
				Dist d;
				if (grid.simpleDist(pos, g.pos, d))
					fd = std::min(fd, d);
			}
			if (bd > 4 && fd < friendDist) {
				move.offset = s;
				friendDist = fd;
				bitchDist = bd;
			}
			if (friendDist < MAX_SIZE)
				continue;
			if (fd < 2 && ((!foundSafe && bd > 1) || bd > bitchDist)) {
				move.offset = s;
				bitchDist = bd;
				foundSafe = true;
			}
			if (foundSafe)
				continue;
			if (bd > bitchDist) {
				move.offset = s;
				bitchDist = bd;
			}
		}
		return move;
	}
};

template<typename S>
struct PlanStrategy {
	S strategy;
	Move solve(const State& state, size_t idx) const {
		Move move = strategy.solve(state, idx);
		if (move.offset != Step::WAIT || move.effect != Effect::NONE)
			return move;
		auto& me = state.guys[idx];
		if (me.plans == 0 || me.hp >= 200 || me.currentEffect != Effect::NONE)
			return move;
		Dist dist = MAX_SIZE;
		for (auto& w : state.wanderers) {
			Dist d;
			if (!grid.simpleDist(me.pos, w.pos, d))
				continue;
			dist = std::min(dist, d);
		}
		if (dist > 3)
			return { Step::WAIT, Effect::PLAN };
		return move;
	}
};

const static Move KNOWN_MOVES[] = {
	{ Step::WAIT, Effect::NONE },
	{ Step::UP, Effect::NONE },
	{ Step::DOWN, Effect::NONE },
	{ Step::LEFT, Effect::NONE },
	{ Step::RIGHT, Effect::NONE },
	{ Step::WAIT, Effect::PLAN },
	{ Step::WAIT, Effect::LIGHT },
	{ Step::WAIT, Effect::YELL },
};

template<typename S>
struct Genius {
	S strategy;

	Move solve1(const State& state, size_t idx) const {
		Move moves[4];
		for (size_t i = 0; i < 4; i++) {
			if (i == idx || state.guys[i].hp == 0)
				continue;
			moves[i] = strategy.solve(state, i);
		}
		Move basicMove = strategy.solve(state, idx);
		Move move = basicMove;
		moves[idx] = move;
		Hp fitness = eval(state, moves, idx);
		for (Move m : KNOWN_MOVES) {
			if (m.offset == basicMove.offset && m.effect == basicMove.effect)
				continue;
			moves[idx] = m;
			Hp f = eval(state, moves, idx);
			if (f > fitness) {
				move = m;
				fitness = f;
			}
		}
		return move;
	}

	Move solve(const State& state, size_t idx) const {
		Alarm hmmm(40 * 1000 * 1000);
		Alarm fail(48 * 1000 * 1000);
		Move defMoves[4];
		Move moves[8];
		prepare(state, idx, defMoves, moves, 8);
		Move best = moves[0];
		Hp fitness = -30000;
		for (Move m1 : moves) {
			State s1 { state };
			defMoves[idx] = m1;
			simulate(s1, defMoves);
			Move defMoves1[4];
			Move moves1[8];
			prepare(s1, idx, defMoves1, moves1, 8);
			for (Move m2 : moves1) {
				State s2 { s1 };
				defMoves1[idx] = m2;
				simulate(s2, defMoves1);
				Move defMoves2[4];
				Move moves2[5];
				prepare(s2, idx, defMoves2, moves2, 5);
				for (Move m3 : moves2) {
					if (fail.finish())
						return best;
					State s3 { s2 };
					defMoves2[idx] = m3;
					simulate(s3, defMoves2);
					for (size_t i = 0; i < 0; i++)
						run(s3);
					Hp f = fit(s3, idx);
					if (f > fitness) {
						fprintf(stderr, "Moves: %3d-%d %3d-%d %3d-%d fitness: %4d\n",
								(int)m1.offset, (int)m1.effect,
								(int)m2.offset, (int)m2.effect,
								(int)m3.offset, (int)m3.effect,
								(int)f
						);
						fitness = f;
						best = m1;
					}
					if (hmmm.finish())
						break;
				}
			}
		}
		return best;
	}

	void prepare(const State& state, size_t idx, Move moves[4], Move* moves2, size_t cnt) const {
		for (size_t i = 0; i < 4; i++)
			moves[i] = strategy.solve(state, idx);
		for (size_t i = 0; i < cnt; i++) {
			moves2[i] = KNOWN_MOVES[i];
			if (moves2[i].offset == moves[idx].offset && moves2[i].effect == moves[idx].effect) {
				for (size_t j = i; j > 0; j--)
					moves2[j] = moves2[j - 1];
				moves2[0] = moves[idx];
			}
		}
	}

	Hp eval(const State& state, const Move moves[4], size_t idx) const {
		State newState{ state };
		simulate(newState, moves);
		for (int i = 0; i < 5; i++)
			run(newState);
		return fit(newState, idx);
	}

	Hp fit(const State& state, size_t idx) const {
		auto& g = state.guys[idx];
		Hp newHp = g.hp;
		newHp += std::max(30 - (Tick)(state.tick + 1) / 2, 0) * g.plans;
		Dist fr = MAX_SIZE;
		for (size_t i = 0; i < 4; i++) {
			if (i == idx)
				continue;
			newHp -= state.guys[i].hp / 4;
			Dist d;
			if (grid.simpleDist(g.pos, state.guys[i].pos, d))
				fr = std::min(fr, d);
		}
		//newHp -= std::min<Dist>(fr, 5) + fr / 5;
		//for (auto& w : state.wanderers) {
			//Dist d;
			//if (grid.simpleDist(g.pos, w.pos, d)) {
				//if (d < 5)
					//newHp--;
				//if (d < 3)
					//newHp--;
				//if (d < 2)
					//newHp--;
			//}
		//}
		return newHp;
	}

	void run(State& state, Move move, size_t idx) const {
		Move moves[4];
		moves[idx] = move;
		for (size_t i = 0; i < 4; i++) {
			if (i != idx && state.guys[i].hp > 0)
				moves[i] = strategy.solve(state, i);
		}
		simulate(state, moves);
	}

	void run(State& state) const {
		Move moves[4];
		for (size_t i = 0; i < 4; i++) {
			if (state.guys[i].hp > 0)
				moves[i] = strategy.solve(state, i);
		}
		simulate(state, moves);
	}
};

int main() {
	int width;
	std::cin >> width; std::cin.ignore();
	int height;
	std::cin >> height; std::cin.ignore();
	for (int y = 0; y < height; y++) {
		std::string line;
		std::getline(std::cin, line);
		for (int x = 0; x < width; x++) {
			field[parse(x, y)] = (Cell)line[x];
		}
		if (y == 0 || y == height - 1) {
			for (int x = 0; x < width; x++)
				field[parse(x, y)] = Cell::WALL;
		}
		field[parse(0, y)] = Cell::WALL;
		field[parse(width - 1, y)] = Cell::WALL;
	}
	int lossAloneInt, lossGroupInt, wandererSpawnInt, wandererTtlInt;
	std::cin >> lossAloneInt >> lossGroupInt >> wandererSpawnInt >> wandererTtlInt; std::cin.ignore();

	lossAlone = lossAloneInt;
	lossGroup = lossGroupInt;
	wandererSpawn = wandererSpawnInt;
	wandererTtl = wandererTtlInt;
	grid.init(height, width);
	Tick tick = -1;
	bool spawnedSlasher[4] = { false, false, false, false };
	uint16_t yellMask = 0;
	std::unordered_map<Id, Point> slasherTargets;

	// game loop
	while (true) {
		int entityCount; // the first given entity corresponds to your explorer
		std::cin >> entityCount; std::cin.ignore();
		State state { };
		state.tick = tick;
		for (int i = 0; i < entityCount; i++) {
			std::string entityType;
			int id;
			int x;
			int y;
			int param0;
			int param1;
			int param2;
			std::cin >> entityType >> id >> x >> y >> param0 >> param1 >> param2; std::cin.ignore();
			state.nextId = std::max<Id>(state.nextId, id + 1);
			if (entityType == "EXPLORER") {
				size_t idx = state.getGuyIdx(id);
				auto& g = state.guys[idx];
				g.id = id;
				g.pos = parse(x, y);
				g.hp = param0;
				g.plans = param1;
				g.lights = param2;
				state.guys[idx].spawnedSlasher = spawnedSlasher[idx];
			}
			else if (entityType == "WANDERER") {
				state.wanderers.emplace_back();
				auto& w = state.wanderers.back();
				w.id = id;
				w.pos = parse(x, y);
				w.ttl = param0 + tick;
				w.state = (BitchState) param1;
				w.target = param2;
			}
			else if (entityType == "SLASHER") {
				state.slashers.emplace_back();
				auto& s = state.slashers.back();
				s.id = id;
				s.pos = parse(x, y);
				s.ttl = tick + param0;
				s.state = (BitchState) param1;
				s.target = param2;
				if (s.state == BitchState::SPAWNING && param0 == 5) {
					size_t idx = state.getGuyIdx(s.target);
					spawnedSlasher[idx] = true;
					state.guys[idx].spawnedSlasher = true;
				}
			}
			else if (entityType == "EFFECT_PLAN" || entityType == "EFFECT_LIGHT") {
				auto& g = state.getGuy(param1);
				if (entityType == "EFFECT_PLAN")
					g.currentEffect = Effect::PLAN;
				if (entityType == "EFFECT_LIGHT")
					g.currentEffect = Effect::LIGHT;
				g.effectTtl = tick + param0;
			}
			else if (entityType == "EFFECT_YELL") {
				Tick ttl = tick + param0;
				auto& gg = state.getGuy(param2);
				gg.yelledTtl = ttl + 1;
				yellMask |= 1 << (4 * state.getGuyIdx(param1) + state.getGuyIdx(param2));
			}
			else if (entityType == "EFFECT_SHELTER") {
				state.shelters.push_back({ parse(x, y), (Amount) param0 });
			}
			else {
				throw "HOLY SHIT!";
			}
		}
		for (auto& s : state.slashers) {
			if (s.target != -1 && grid.los(s.pos, slasherTargets[s.id]))
				slasherTargets[s.id] = state.getGuy(s.target).pos;
			if (s.state == BitchState::STALKING)
				s.targetPos = slasherTargets[s.id];
			else
				s.targetPos = s.pos;
		}
		state.yellMask = yellMask;
		tick++;

		Genius<Idiot> strategy;
		Timer total;
		Move move = strategy.solve(state, 0);
		Time timeSpent = total.get();
		fprintf(stderr, "Time spent: %2d %03d %03d", (int) (timeSpent / 1000000), (int) (timeSpent / 1000 % 1000), (int)(timeSpent % 1000));
		switch (move.offset)
		{
		case Step::UP:
			std::cout << "UP" << std::endl;
			break;
		case Step::DOWN:
			std::cout << "DOWN" << std::endl;
			break;
		case Step::LEFT:
			std::cout << "LEFT" << std::endl;
			break;
		case Step::RIGHT:
			std::cout << "RIGHT" << std::endl;
			break;
		case Step::WAIT:
			switch (move.effect)
			{
			case Effect::PLAN:
				std::cout << "PLAN" << std::endl;
				break;
			case Effect::LIGHT:
				std::cout << "LIGHT" << std::endl;
				break;
			case Effect::YELL:
				std::cout << "YELL" << std::endl;
				break;
			case Effect::NONE:
				std::cout << "WAIT" << std::endl;
			}
		}
	}
}
